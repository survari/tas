#** TAS - Ein Akten-System für Minecraft Server **#

TAS ist ein einfaches Akten-System um Usern eigene Akten zu geben. In diesen Akten können dann Einträge zum Verhalten gemacht werden. Sollte sich ein User schlecht verhalten, gibt es einen Eintrag in die Akte. Per ID
kann man die Einträge aus der Akte eines Users auslesen.

##** Wie installiere ich es auf meinem Minecraft Server? **##

Einfach wie alle anderen Plugins ab in den `plugins/` Ordner! Beim ersten Starten wird im Server ein Ordner namens `TASRecords` angelegt. Dort sind alle Akten zu jeweiligen Usern gespeichert.

## **Crashkurs** ##

####**Eintrag in Akte hinzufuegen**####

Eintraege in Akten kann man mit `/tas a <Spieler> <Eintrag-ID> "<Nachricht>"` Eintragen.
Das `a` steht dabei für `add` (hinzufügen), statt `<Spieler>` muss der Namen des Spielers eingetragen werden und statt `<Eintrag-ID>` eine Zahl, mit der man den Eintrag in die Akte nachher abrufen kann. Wichtig ist beim Befehl, dass die Nachricht in Anführungszeichen (") geschrieben wird, sonst gibt es Probleme und Bugs, die auf das Argumentesystem von Minecraft zurückzuführen sind.

Existiert eine Akte nicht, wird eine Akte erstellt.
Lässt man die ID weg, wird eine zufällige genommen.

Beispiel für einen Befehl:

    /tas a Survari 1 "Survari ist der Entwickler des TA-Systems (TAS)."
Damit wird ein Eintrag mit der ID 1 erstellt.
Wie wir diesen Beitrag abrufen können, sieht man hier...

####**Eintrag aus Akte auslesen**####

Den Eintrag den wir gerade erstellt haben, können wir wie folgt auslesen:

    /tas s Survari 1

Ausgegeben wird dann `"Survari ist der Entwickler des TA-Systems (TAS)."`.
Erstellen wir nun einen Beitrag, von dem wir die ID nicht wissen (lassen die ID also weg und lassen sie random generieren).

    /tas a Survari "Wie komme ich an diese Nachricht?"

Wir haben nun also einen Eintrag erstellt, von dem die ID zufällig generiert wurde.
Lesen wir nun also alle Beiträge aus um die ID des neuen Eintrags herauszufinden:

    /tas s Survari

Damit werden alle IDs ausgegeben: `Folgende Einträge gefunden: 1 5466`.  
Eintrag 5466 muss wohl der neue sein.  
Um alle User mit allen Akten anzuzeigen, kann mann `/tas s` nutzen.
  
PS: `s` steht für `see`.

####**Einträge entfernen**####

Das geht ziemlich einfach: `/tas r <Spieler> <Eintrag-ID>`.  
Es ist auch möglich den Spieler komplett aus dem System zu entfernen: `/tas r <Spieler>`.   
Das `r` steht hierbei für `remove`.  
Mehr muss ich da glaube ich nicht erklären.

##**WICHTIG: Nach einem Neustart ist alles futsch! Oder doch nicht ...?**##

Wer mal versuch einen Eintrag zu erstellen, den Server dann zu reloaden/neuzustarten, der wird darauf stoßen, dass der erstellte Eintrag weg ist.  
Das liegt ganz einfach daran, dass die Änderungen die geschehen nicht sofort übernommen werden. Alle Einträge werden in dem `TASRecords` Ordner gespeichert. Wer also einen Eintrag erstellt, erstellt nicht sofort eine neue Datei, da dies zu Ressourcenintensiv und zu gefährlich währe.  

Nehmen wir mal an, wir haben einen ganz bösen User auf dem Server und dessen Akte ist randvoll gefüllt. Außversehen löscht ein Admin die komplette Akte. Wenn dies sofort auf die Festplatte geschrieben wird, hätte man ein Problem. Deshalb wird es nur beim Herunterfahren des Servers (`/stop`) oder beim Neuladen gespeichert.  

Natürlich gibt es auch einen manuellen weg die aktuellen Änderungen auf die Festplatte zu schreiben, undzwar mit `/tas save`. Das ist dann aber auch unabänderlich der letzte Schlussstrich.  

Wenn der Admin die Akte also gelöscht hat, gesehen hat was er getan hat, kann er die Akten einfach alle neu von der Festplatte in den Speicher des Servers laden: `/tas reload`. Damit werden dann alle Akten neu eingelesen und die ganzen Einträge in der Akte des bösen Users sind wieder da!

##**Permissions**##

| Permission         | Beschreibung                            | Befehl                                         |  
| ------------------ | --------------------------------------- | ---------------------------------------------- |  
| `tas.help`         | Hilfe anzeigen.                         | `/tas help`                                    | 
| `tas.*`            | Alle Rechte auf das Plugin.             |                                                |
| `tas.remove.entry` | Einen Eintrag entfernten.               | `/tas r <Spieler> <Eintrags-ID>`               |
| `tas.remove.all`   | Eine komplette Akte entfernen.          | `/tas r <Spieler>`                             |
| `tas.add`          | Einen Eintrag hinzufügen.               | `/tas a <Spieler> <Eintrags-ID> "<Nachricht>"` |
|                    |                                         | `/tas a <Spieler> "<Nachricht>"`               |
| `tas.see.entry`    | Einen Eintrag ansehen.                  | `/tas s <Spieler> <Eintrags-ID>`               |
| `tas.see.entries`  | Alle Einträge (IDs) anzeigen.           | `/tas s <Spieler>`                             |
| `tas.see.all`      | Alle User mit allen Einträgen anzeigen. | `/tas s`                                       |
| `tas.save`         | Alle Änderungen Speichern.              | `/tas save`                                    |
| `tas.reload`       | Alle Akten neu laden.                   | `/tas reload`                                  |