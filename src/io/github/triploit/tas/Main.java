package io.github.triploit.tas;

import io.netty.internal.tcnative.Buffer;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import org.omg.SendingContext.RunTime;
import org.yaml.snakeyaml.Yaml;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main extends JavaPlugin
{
    @Override
    public void onEnable()
    {
        System.out.println("[ TAS ] Info: Kern geladen.\n");
        System.out.println("[ TAS ] Info: Lade alle Akten (falls vorhanden) ...\n\tLade in Verzeichnis: "+(new File(".").getAbsolutePath()));


        if (!(new File(Runtime.TAS_DIR)).exists())
        {
            System.out.println("[ TAS ] Info: Plugin das erste mal gestartet. Initialisiere noetige Komponenten...");
            (new File(Runtime.TAS_DIR)).mkdirs();
        }
        else
        {
            for (File f : (new File(Runtime.TAS_DIR)).listFiles())
            {
                try
                {
                    String in = "";

                    if (!f.getName().endsWith(".rec"))
                    {
                        System.out.println("[ TAS ] Info: Uebersprungene Datei: "+f.getName());
                        continue;
                    }
                    else System.out.println("[ TAS ] Info: Datei: "+f.getName());

                    File file = new File(f.getAbsolutePath());
                    FileInputStream fis = new FileInputStream(file);
                    byte[] data = new byte[(int) file.length()];

                    fis.read(data);
                    fis.close();

                    in = new String(data, "UTF-8");
                    System.out.println("");

                    Runtime.loadRecord(in, f.getName());
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                    return;
                }
            }
        }
    }

    @Override
    public void onDisable()
    {
        System.out.println("[ TAS ] Info: Sichere alle änderungen...");
        Runtime.writeRecords();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] targs)
    {
        if (command.getName().equalsIgnoreCase("tas"))
        {
            List<String> args = new ArrayList<String>();
            String chat_message = "";

            for (String s : targs) chat_message = chat_message + s + " ";
            boolean str = false;
            String tmp = "";

            for (int i = 0; i < chat_message.length(); i++)
            {
                if (chat_message.charAt(i) == '"')
                {
                    if (str) str = false;
                    else str = true;
                }

                if (str)
                {
                    tmp += chat_message.charAt(i);
                }
                else
                {
                    if (chat_message.charAt(i) == ' ')
                    {
                        if (tmp != "" && !tmp.isEmpty())
                        {
                            args.add(tmp);
                            tmp = "";
                        }
                    }
                    else tmp += chat_message.charAt(i);
                }
            }

            if (tmp != "" && !tmp.isEmpty())
                args.add(tmp);

            if (args.size() == 0)
            {
                if ((sender.hasPermission("tas.*") || sender.hasPermission("tas.help")))
                {
                    sender.sendMessage(Runtime.help);
                }
                else Runtime.noPermission(sender);
            }
            else if (args.size() == 2)
            {
                String cmd = args.get(0);
                String player = args.get(1);

                if (cmd.equals("s"))
                {
                    if (sender.hasPermission("tas.*") || sender.hasPermission("tas.see.entries"))
                    {
                        if (Runtime.existsRecord(player))
                        {
                            sender.sendMessage(ChatColor.GREEN+"[ TAS ] Folgende Einträge gefunden: "+Runtime.getIDs(player));
                        }
                        else
                            sender.sendMessage(ChatColor.RED+"[ TAS ] Akte existiert nicht.");
                    }
                }
                else if (cmd.equals("r"))
                {
                    if (sender.hasPermission("tas.*") || sender.hasPermission("tas.remove.all"))
                    {
                        if (Runtime.existsRecord(player))
                        {
                            Runtime.removeRecord(player);
                            sender.sendMessage(ChatColor.GREEN+"[ TAS ] Akte entfernt. (Erst bei Speicherung wirklich)");
                        }
                        else
                            sender.sendMessage(ChatColor.RED+"[ TAS ] Akte existiert nicht.");
                    }
                    else
                        Runtime.noPermission(sender);
                }
            }
            else if (args.size() == 3)
            {
                String cmd = args.get(0);
                String player = args.get(1);
                String message = args.get(2);

                if (cmd.equals("a"))
                {
                    if (sender.hasPermission("tas.*") || sender.hasPermission("tas.add"))
                    {
                        Runtime.addEntry(player, new Entry(-1, message));
                        sender.sendMessage(ChatColor.GREEN+"[ TAS ] Eintrag für den Spieler "+player+" erstellt!");
                    }
                }
                else if (cmd.equals("s"))
                {
                    if (sender.hasPermission("tas.*") || sender.hasPermission("tas.see.entry"))
                    {
                        if (Runtime.existsRecord(player))
                        {
                            try
                            {
                                int id = Integer.parseInt(message);

                                if (Runtime.getRecord(player).existsEntry(id))
                                {
                                    sender.sendMessage(ChatColor.WHITE+"==== Eintrag: "+id+"\n==== Spieler: "+player+"\n"+Runtime.getEntry(player, id));
                                }
                                else
                                    sender.sendMessage(ChatColor.RED+"[ TAS ] Eintrag nicht gefunden.");

                            }
                            catch(NumberFormatException ex)
                            {
                                sender.sendMessage(ChatColor.RED+"[ TAS ] Keine gültige ID angegeben.");
                            }
                        }
                        else
                            sender.sendMessage(ChatColor.RED+"[ TAS ] Akte existiert nicht.");
                    }
                }
                else if (cmd.equals("r"))
                {
                    if (sender.hasPermission("tas.*") || sender.hasPermission("tas.remove.entry"))
                    {
                        if (Runtime.existsRecord(player))
                        {
                            try
                            {
                                Runtime.removeEntry(player, Integer.parseInt(message));
                                sender.sendMessage(ChatColor.GREEN+"[ TAS ] Gelöscht! (Erst bei Speicherung wirksam)");
                            }
                            catch(NumberFormatException ex)
                            {
                                sender.sendMessage(ChatColor.RED+"[ TAS ] Keine gültige ID angegeben.");
                            }
                        }
                        else
                            sender.sendMessage(ChatColor.RED+"[ TAS ] Akte existiert nicht.");
                    }
                    else
                        Runtime.noPermission(sender);
                }
            }
            else if (args.size() == 4)
            {
                String cmd = args.get(0);
                String player = args.get(1);
                String entry = args.get(2);
                String message = args.get(3);

                if (cmd.equals("a"))
                {
                    if (sender.hasPermission("tas.*") || sender.hasPermission("tas.add"))
                    {
                        try
                        {
                            if (!Runtime.addEntry(player, new Entry(Integer.parseInt(entry), message)))
                                sender.sendMessage(ChatColor.RED+"[ TAS ] Ein Eintrag mit der ID "+entry+" konnte nicht erstellt werden! Existiert er vielleicht schon?");
                            else sender.sendMessage(ChatColor.GREEN+"[ TAS ] Eintrag für "+player+" erstellt!");
                        }
                        catch (NumberFormatException ex)
                        {
                            sender.sendMessage(ChatColor.RED+"[ TAS ] Die ID "+entry+" ist eine ungültige Zahl!");
                        }
                    }
                }
            }
            else if (args.get(0).equalsIgnoreCase("help"))
            {
                if (sender.hasPermission("tas.*") || sender.hasPermission("tas.help"))
                {
                    sender.sendMessage(Runtime.help);
                }
                else
                    Runtime.noPermission(sender);
            }
            else if (args.get(0).equalsIgnoreCase("save"))
            {
                if (sender.hasPermission("tas.*") || sender.hasPermission("tas.save"))
                {
                    Runtime.writeRecords();
                    sender.sendMessage(ChatColor.GREEN+"[ TAS ] OK.");
                }
                else
                    Runtime.noPermission(sender);
            }
            else if (args.get(0).equalsIgnoreCase("s"))
            {
                if (sender.hasPermission("tas.*") || sender.hasPermission("tas.see.all"))
                {
                    for (Record r : Runtime.records)
                    {
                        sender.sendMessage(r.player_name+" hat "+r.entries.size()+" Einträge:\n    - "+Runtime.getIDs(r.player_name));
                    }
                }
                else
                    Runtime.noPermission(sender);
            }
            else if (args.get(0).equalsIgnoreCase("reload"))
            {
                if (sender.hasPermission("tas.*") || sender.hasPermission("tas.reload"))
                {
                    Runtime.to_delete.clear();
                    Runtime.records.clear();

                    for (File f : (new File(Runtime.TAS_DIR)).listFiles())
                    {
                        try
                        {
                            String in = "";

                            if (!f.getName().endsWith(".rec"))
                            {
                                System.out.println("[ TAS ] Info: Uebersprungene Datei: "+f.getName());
                                continue;
                            }
                            else System.out.println("[ TAS ] Info: Datei: "+f.getName());

                            File file = new File(f.getAbsolutePath());
                            FileInputStream fis = new FileInputStream(file);
                            byte[] data = new byte[(int) file.length()];

                            fis.read(data);
                            fis.close();

                            in = new String(data, "UTF-8");
                            System.out.println("");

                            Runtime.loadRecord(in, f.getName());
                        }
                        catch (IOException e)
                        {
                            e.printStackTrace();
                            return true;
                        }
                    }

                    sender.sendMessage(ChatColor.GREEN+"[ TAS ] OK.");
                }
                else
                    Runtime.noPermission(sender);
            }
            else sender.sendMessage(ChatColor.YELLOW+"[ TAS ] Befehl nicht gefunden!");
        }

        return true;
    }
}
