package io.github.triploit.tas;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import javax.annotation.RegEx;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Runtime
{
    /*
        /tas a <Spieler> <Beschreibung>                 -> Fügt einen Eintrag hinzu (Zufällig gewählte ID)
        /tas a <Spieler> <Entry ID> <Beschreibung>      -> Fügt einen Eintrag hinzu

        /tas s <Spieler>                                -> Gibt alle Eintrags-ID's aus.
        /tas s <Spieler> <Entry ID>                     -> Sieht einen Eintrag an

        /tas r <Spieler>                                -> Entfernt alle Einträge eines Spielers
        /tas r <Spieler> <Entry ID>                     -> Entfernt einen Eintrag aus der Akte des Spielers
    */

    public static List<Record> records = new ArrayList<>();
    public static List<String> to_delete = new ArrayList<>();
    public static String TAS_DIR = "./TASRecords";

    public static String help =
            "[]============[ " + ChatColor.AQUA + " TAS Hilfe " +ChatColor.WHITE+ " ]===============[]\nSchau mal hier nach Hilfe.\n\n" +
                    ChatColor.BLUE + "    tas "+ChatColor.RED+" help" + ChatColor.WHITE + "\n      -> Zeigt diese Hilfe an.\n"+
                    ChatColor.BLUE + "    tas "+ChatColor.RED+" a <Spieler> <Beschreibung>" + ChatColor.WHITE + "\n      -> Fügt einen Eintrag mit einer zufälligen ID hinzu.\n"+
                    ChatColor.BLUE + "    tas "+ChatColor.RED+" a <Spieler> <EntryID> <Beschreibung>" + ChatColor.WHITE + "\n      -> Fügt einen Eintrag mit einer selbst festgelegten ID hinzu.\n"+
                    ChatColor.BLUE + "    tas "+ChatColor.RED+" s" + ChatColor.WHITE + "\n      -> Listet alle Spielerakten und deren Einträge (als IDs) auf.\n"+
                    ChatColor.BLUE + "    tas "+ChatColor.RED+" s <Spieler>" + ChatColor.WHITE + "\n      -> Listet die ID's aller Einträge auf.\n"+
                    ChatColor.BLUE + "    tas "+ChatColor.RED+" s <Spieler> <EntryID>" + ChatColor.WHITE + "\n      -> Gibt den Eintrag des Spielers mit der gewünschten ID aus.\n"+
                    ChatColor.BLUE + "    tas "+ChatColor.RED+" r <Spieler>" + ChatColor.WHITE + "\n      -> Entfernt alle Einträge des ausgewählten Spielers aus dem System.\n"+
                    ChatColor.BLUE + "    tas "+ChatColor.RED+" r <Spieler> <EntryID>" + ChatColor.WHITE + "\n      -> Entfernt einen Eintrag eines Spielers mit einer bestimmten ID aus dem System.\n"+
                    ChatColor.BLUE + "    tas "+ChatColor.RED+" save" + ChatColor.WHITE + "\n      -> Speichert alle Änderungen.\n";

    public static void noPermission(CommandSender sender)
    {
        sender.sendMessage(ChatColor.RED+"[ TAS ] Dazu hast du keine Rechte!");
    }

    public static Record getRecord(String name)
    {
        for (Record r : records)
        {
            if (r.player_name.equals(name)) return r;
        }

        return null;
    }

    public static boolean existsRecord(String name)
    {
        for (Record r : records)
        {
            if (name.equals(r.player_name)) return true;
        }

        return false;
    }

    public static boolean addEntry(String name, Entry e)
    {
        boolean b = true;
        boolean found = false;

        for (int i = 0; i < records.size(); i++)
        {
            if (records.get(i).player_name.equals(name))
            {
                found = true;
                Record r = records.get(i);

                if (e.id == -1)
                {
                    while (r.existsEntry(e.id) || e.id == -1)
                    {
                        e.id = ThreadLocalRandom.current().nextInt(1, 99999 + 1);
                    }
                }

                b = r.addEntry(e);
                records.set(i, r);
            }
        }

        if (!found)
        {
            Record r = new Record(name);
            b = r.addEntry(e);
            records.add(r);
        }

        return b;
    }

    public static String getIDs(String name)
    {
        String s = "";

        if (existsRecord(name))
        {
            for (Entry e : getRecord(name).entries) s += e.id + " ";
        }
        else return null;
        return s;
    }

    public static String getEntry(String name, int id)
    {
        if (existsRecord(name))
        {
            for (Entry e : getRecord(name).entries)
            {
                if (e.id == id) return e.message;
            }
        }
        else return null;
        return null;
    }

    public static boolean removeEntry(String name, int id)
    {
        for (int i = 0; i < records.size(); i++)
        {
            if (records.get(i).player_name.equals(name))
            {
                Record r = records.get(i);
                if (r.existsEntry(id)) r.removeEntry(id);
                records.set(i, r);
                return true;
            }
        }

        return false;
    }

    public static boolean removeRecord(String name)
    {
        records.remove(name);
        to_delete.add(name);

        for (int i = 0; i < records.size(); i++)
        {
            if (records.get(i).player_name.equals(name))
            {
                to_delete.add(name);
                records.remove(i);
            }
        }

        return true;
    }

    public static void loadRecord(String in, String name)
    {
        String[] lines = in.split("\n");
        Record r = new Record(name.replace(".rec", ""));
        String regex = "([0-9]+)( +|\t+)(.+)";
        Pattern p = Pattern.compile(regex);
        int lc = 1;

        for (String line : lines)
        {
            if (line.trim().startsWith("#") ||line.trim().startsWith("//") || line.trim().startsWith(";")) continue;

            Matcher m = p.matcher(line);

            if (m.find())
            {
                r.addEntry(new Entry(Integer.parseInt(m.group(1)), m.group(3)));
            }
            else System.out.println("           Info: Error in Datei \""+name+"\": in Zeile "+lc+"\n\t"+line);
            lc++;
        }

        records.add(r);
    }

    public static void writeRecords()
    {
        for (Record r : records)
        {
            System.out.println("[ TAS ] Info: Speichere Akte von: "+r.player_name);
            try
            {
                writeRecord(r);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            deleteOldRecords();
        }
    }

    private static void deleteOldRecords()
    {
        for (File f : (new File(TAS_DIR)).listFiles())
        {
            for (String s : to_delete)
            {
                if (f.getName().equals(s+".rec"))
                {
                    System.out.println("[ TAS ] Info: Loesche Datei: "+s+".rec");
                    f.delete();
                }
            }
        }

        Runtime.to_delete.clear();
    }

    private static void writeRecord(Record r) throws IOException
    {
        boolean found = false;

        for (File f : (new File(TAS_DIR)).listFiles())
        {
            if ((r.player_name+".rec").equals(f.getName()))
            {
                found = true;
                f.delete();
                f.createNewFile();

                try
                {
                    PrintWriter writer = new PrintWriter(f.getAbsolutePath(), "UTF-8");
                    for (Entry e : r.entries) writer.println(e.id+" "+e.message);
                    writer.close();
                }
                catch (UnsupportedEncodingException ex)
                {
                    ex.printStackTrace();
                }
                catch(FileNotFoundException ex)
                {
                    ex.printStackTrace();
                }
            }
        }

        if (!found)
        {
            System.out.println("[ TAS ] Info: Neue Datei wird erstellt: "+TAS_DIR+"/"+r.player_name+".rec");
            (new File(TAS_DIR+"/"+r.player_name+".rec")).createNewFile();

            try
            {
                PrintWriter writer = new PrintWriter(TAS_DIR+"/"+r.player_name+".rec", "UTF-8");
                for (Entry e : r.entries) writer.println(e.id+" "+e.message);
                writer.close();
            }
            catch (UnsupportedEncodingException ex)
            {
                ex.printStackTrace();
            }
            catch(FileNotFoundException ex)
            {
                ex.printStackTrace();
            }
        }
    }
}
