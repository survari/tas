package io.github.triploit.tas;

public class Entry
{
    public int id = -1;
    public String message;

    public Entry(int id, String message)
    {
        this.id = id;
        this.message = message;
    }
}
