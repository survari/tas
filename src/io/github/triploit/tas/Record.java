package io.github.triploit.tas;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Record
{
    public String player_name;
    public List<Entry> entries = new ArrayList<>();

    public boolean existsEntry(int id)
    {
        for (Entry i : entries) if (i.id == id) { return true; }
        return false;
    }

    public boolean addEntry(Entry i)
    {
        if (!existsEntry(i.id))
            entries.add(i);
        else return false;
        return true;
    }

    public Record(String name, Entry[] es)
    {
        this.entries = Arrays.asList(es);
        this.player_name = name;
    }

    public Record(String name)
    {
        this.player_name = name;
    }

    public void removeEntry(int id)
    {
        for (int i = 0; i < entries.size(); i++)
        {
            if (entries.get(i).id == id) entries.remove(i);
        }
    }
}
